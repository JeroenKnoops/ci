#!/usr/bin/env bash

source $(dirname "${BASH_SOURCE[0]}")/environment.sh

showResult "Assemble"

count=`ls -1 build/lib/*.jar 2>/dev/null | wc -l`
if [ $count != 0 ]
then
    echo "Skipping assemble, artifacts already exists."
    find build/lib -name "*.jar"
else
    if ./gradlew assemble ${EXTRA_GRADLE_ARGS} $@; then
      showResult "Done"
    else
      showResult "Failed"
      exit 1
    fi
fi
