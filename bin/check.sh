#!/usr/bin/env bash

source $(dirname "${BASH_SOURCE[0]}")/environment.sh

showResult "Check"

count=`ls -1 build/libs/*.jar 2>/dev/null | wc -l`
if [ $count != 0 ]
then
    echo "Artifacts exists. That's a good thing! Let's check them."
    if ./gradlew check ${EXTRA_GRADLE_ARGS} $@; then
      showResult "Done"
    else
      showResult "Failed"
      exit 1
    fi
else
    echo "No artifacts found. That's not good."
    showResult "Failed"
    exit 1
fi
