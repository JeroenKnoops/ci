#!/usr/bin/env bash

usage() {
	printf "Usage: \n"
	printf "  -i <docker image name> or set environment variable DOCKER_IMAGE=$DOCKER_IMAGE \n"
	printf "  -r <docker registry name> or set environment variable DOCKER_REGISTRY=$DOCKER_REGISTRY \n"
	printf "  -t <docker tag> or set environment variable DOCKER_TAG=$DOCKER_TAG \n"
	printf "  -v <version> or set environment variable VERSION=$VERSION \n"
	printf "  -u <gitlab username> or set environment variable GITLAB_USERNAME=$GITLAB_USERNAME \n"
	printf "  -p <gitlab password> or set environment variable GITLAB_PASSWORD \n"
}

status() {
	printf "Variables set to: \n"
	printf "  DOCKER_IMAGE=$DOCKER_IMAGE \n"
	printf "  DOCKER_REGISTRY=$DOCKER_REGISTRY \n"
	printf "  DOCKER_TAG=$DOCKER_TAG \n"
	printf "  VERSION=$VERSION \n"
	printf "  GITLAB_USERNAME=$GITLAB_USERNAME \n"
}

showResult() {
    hash figlet &> /dev/null
    if [ $? -eq 1 ]; then
        echo >&2 "$1"
    else
        figlet -f small $1
    fi
}

jiraDockerTag() {
    # check on feature bracnh and no tag
    if  [[ ${CI_COMMIT_REF_NAME} =~ ^(feature|bugfix)+/([[:upper:]]+-[0-9]+)-(.*)$ ]] ; then
        export JIRA_ID=${BASH_REMATCH[2]}
        echo Setting DOCKER_TAG to jira id: ${JIRA_ID}
        export DOCKER_TAG=${JIRA_ID}
    fi
}

setDockerTag() {
		# set latest as default tag
    if  [[ -z ${DOCKER_TAG}  ]] ; then
				export DOCKER_TAG=latest
		fi

		# check feature branch or bugfix branch can be used for tagging
		jiraDockerTag

    # overwrite in case git tag is available
    if  [[ ! -z ${CI_COMMIT_TAG}  ]] ; then
        echo Setting VERSION and DOCKER_TAG to ${JIRA_ID}
        export VERSION=${CI_COMMIT_TAG}
        export DOCKER_TAG=${CI_COMMIT_TAG}
    fi
}

setRepositoryCredentials() {
		# set Default gitlab pull push, user
    if  [[ -z ${GITLAB_USERNAME}  ]] ; then
				export GITLAB_USERNAME=gitlab-ci-token
		fi

		if  [[ -z ${GITLAB_PASSWORD}  ]] ; then
				export GITLAB_PASSWORD=$CI_BUILD_TOKEN
		fi
}

toolsInfo() {
	_currentdir=$(pwd)
	cd $(dirname "${BASH_SOURCE[0]}")
	export CI_VERSION=$(git rev-parse --short HEAD)
  git fetch
  export HEADHASH=$(git rev-parse HEAD)
  export UPSTREAMHASH=$(git rev-parse master@{upstream})

	cat .build.txt
	cd $_currentdir
	echo ""
	echo " CI scripts version : ${CI_VERSION}"

  NOCOLOR='\033[0m' # No Color
  ERROR='\033[1;31m'
  FINISHED='\033[1;96m'
  if [ "$HEADHASH" != "$UPSTREAMHASH" ]
  then
    echo -e ${ERROR} -------------------------------------------------------------${NOCOLOR}
    echo -e ${ERROR} Not up to date with origin. New version available.${NOCOLOR}
    echo -e ${ERROR} -------------------------------------------------------------${NOCOLOR}
    echo -e "${ERROR} CI scripts HEAD    : ${HEADHASH}${NOCOLOR}"
    echo -e "${ERROR} CI scripts upstream: ${UPSTREAMHASH}${NOCOLOR}"
    echo -e ${ERROR} -------------------------------------------------------------${NOCOLOR}
  else
    echo -e ${FINISHED} Current branch is up to date with origin/master.${NOCOLOR}
  fi

	echo ""
	echo ""
}

setDockerTag
setRepositoryCredentials

while getopts ":i:r:t:v:u:p:h" opt; do
  case $opt in
    i)
      export DOCKER_IMAGE=${OPTARG:-${DOCKER_IMAGE}}
      ;;
    r)
      export DOCKER_REGISTRY=${OPTARG:-${DOCKER_REGISTRY}}
      ;;
    t)
      export DOCKER_TAG=${OPTARG:-${DOCKER_TAG}}
      ;;
    v)
      export VERSION=${OPTARG:-${VERSION}}
      ;;
		u)
			export GITLAB_USERNAME=${OPTARG:-${GITLAB_USERNAME}}
			;;
		p)
			export GITLAB_PASSWORD=${OPTARG:-${GITLAB_PASSWORD}}
			;;
    h)
      usage
      ;;
  esac
done
shift $((OPTIND-1))

toolsInfo
status
