#!/usr/bin/env bash

source $(dirname "${BASH_SOURCE[0]}")/environment.sh


count=`ls -1 build/libs/*.jar 2>/dev/null | wc -l`
if [ $count != 0 ]
then
    echo "Skipping build, artifacts already exists."
    find build/libs -name "*.jar"
else
    ./gradlew build generateLicenseReport ${EXTRA_GRADLE_ARGS} $@
fi
