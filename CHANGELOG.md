# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

- [#1] : Checks for new version of CI scripts
- Initial commit

[Unreleased]: https://gitlab.com/jeroen.knoops/ci/compare/0.0.0...develop
