# CI Scripts

Repo contains scripts, templates and utility for building stuff. 

## Scripts (bin)
```
.
├── assemble.sh
├── build.sh
├── check.sh
└── environment.sh
```

- `environment.sh` : Set all the defaults for the build environment.
- `build.sh` : Gradle build.
- `assemble.sh` : Gradle assemble.
- `check.sh` : Gradle check.

# Example usage

## Git submodule
The ci tools are typically added as git submodule to your repo.

### Add the module to your project
```
git submodule add  -b master https://gitlab.com/jeroenknoops/ci.git ci/shared
```

### Update / sync
```
git submodule update --init --recursive --remote
```


## Scripts


# Changelog

See [Changelog][changelog] for more info.

Contributors:
* [Jeroen Knoops](@jeroenknoops)

[changelog]: CHANGELOG.md
